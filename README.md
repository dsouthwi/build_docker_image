This project shows how to use GitLab CI to build a Docker image and upload it to its corresponding place the GitLab registry or to any other Docker registry.

# Overview
With GitLab CI, it is possible to build a Docker image from a Dockerfile kept in a GitLab repository and upload it to the GitLab registry (default case) or to any other Docker registry.
This simplifies the development of Docker images as the process of building and uploading them is triggered by a push to the Dockerfile.

There are several methods users can choose to build their Docker-builds.

The [`.gitlab-ci.yml`](../.gitlab-ci.yml) provided in this example is a simple template that builds the [`Dockerfile`](../Dockerfile)
kept in the project and uploads it to https://gitlab.cern.ch/gitlabci-examples/build_docker_image/container_registry with our different available methods, listed below.

**We recommend adding [container scanning for known vulnerabilities](https://docs.gitlab.com/ee/user/application_security/container_scanning/) on your built Docker images, which you can configure as in <https://gitlab.cern.ch/gitlabci-examples/container_scanning>**.

## Using Kaniko

**This is the recommended method as of May 2019.**

This method uses [Kaniko](https://github.com/GoogleContainerTools/kaniko) for building your images. For upstream documentation please check <https://docs.gitlab.com/ee/ci/docker/using_kaniko.html>.

This Docker-build method can run in our default runners.

We recommend using the CERN version of the Kaniko image: `gitlab-registry.cern.ch/ci-tools/docker-image-builder`. Check <https://gitlab.cern.ch/ci-tools/docker-image-builder> for further details about the image.

## Using Privileged runners

Privileged runners [have access to the host Docker daemon](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding) and can use the [Docker-in-Docker service](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor).

This allows users to build images directly in the CI job scripts and have total control of their builds.

## Docker-build runners (deprecated)

**This method is deprecated as of May 2019** and is only kept for backwards compatibility. If possible please use [Kaniko](#using-kaniko).

To do Docker-builds in a secure way, custom shared runners have been configured and made available for this task. 
The user just need to make the job be executed in any of this runners by adding the [tag](https://gitlab.cern.ch/help/ci/yaml/README.md#tags) 
`docker-image-build`. Mind that within this job, you can not execute any action different than building and uploading the Docker image.  
The custom runners will make use of a provided [docker image](https://gitlab.cern.ch/ci-tools/docker-image-builder/tree/no_kaniko) that includes 
the needed building tools and invokes automatically the build process not allowing the execution of any other command.  

**By default**, the process will build your Docker image and upload it to `gitlab-registry.cern.ch/your_projects_namespace/your_project_name`  
To upload the resulting docker images to a **different registry** than the GitLab registry or to **customize the build process**,  
a number of [variables](https://gitlab.cern.ch/help/ci/variables/README.md) have to be provided:  
* `TO`: a tag to apply to the resulting image. If specified, we'll try to push the image after the build, otherwise, it will try to push the image to the GitLab registry.  
* `DOCKER_LOGIN_SERVER`: a docker registry to login into (allows to provide credentials for pushing the image). By default, `gitlab-registry.cern.ch`  
When uploading the image to a **different registry than the GitLab registry**, credentials are needed and can be provided via [secure variables](https://gitlab.cern.ch/help/ci/variables/README.md#user-defined-variables-secure-variables). Otherwise, these variables are not needed:  
* `DOCKER_LOGIN_USERNAME`: username for login into the docker registry (allows to provide credentials for pushing the image)
* `DOCKER_LOGIN_PASSWORD`: password for login into the docker registry (allows to provide credentials for pushing the image)

Many other variables are available to customize your Docker build like adding additional build parameters. 
The full list of them and their description is available in the [`docker-image-builder` Readme page](https://gitlab.cern.ch/ci-tools/docker-image-builder/tree/no_kaniko).
