FROM cern/cc7-base

MAINTAINER borja.aparicio.cotarelo@cern.ch
ARG BUILDARG
RUN echo I received "$BUILDARG" from kaniko!
COPY helloworld.sh /
RUN chmod 700 /helloworld.sh

ENTRYPOINT /helloworld.sh
